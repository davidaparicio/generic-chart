#!/bin/bash

set -e

composants=("hasher" "rng" "webui" "worker" "redis")

for file in "${composants[@]}"; do
  helm uninstall "$file"
done