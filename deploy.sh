#!/bin/bash

set -e

composants=("redis" "hasher" "rng" "worker" "webui")

for file in "${composants[@]}"; do
  helm upgrade --install "$file" . --values="$file.yaml"
done